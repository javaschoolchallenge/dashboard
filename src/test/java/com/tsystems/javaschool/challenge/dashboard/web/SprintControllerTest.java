package com.tsystems.javaschool.challenge.dashboard.web;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.oneOf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class SprintControllerTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getStarted() throws Exception {
        mvc.perform(get("/sprint"))
                .andExpect(status().isOk())
                .andExpect(content().string(oneOf("true", "false")));
    }

    @Test
    public void startSprint() throws Exception {
        mvc.perform(post("/sprint/start"))
                .andExpect(status().isCreated());
        mvc.perform(get("/sprint"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    public void stopSprint() throws Exception {
        mvc.perform(post("/sprint/start"))
                .andExpect(status().isCreated());
        mvc.perform(get("/sprint"))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
        mvc.perform(post("/sprint/stop"))
                .andExpect(status().isCreated());
        mvc.perform(get("/sprint"))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));
    }

}
