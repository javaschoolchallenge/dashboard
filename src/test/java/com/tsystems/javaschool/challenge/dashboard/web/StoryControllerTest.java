package com.tsystems.javaschool.challenge.dashboard.web;

import com.tsystems.javaschool.challenge.dashboard.domain.Employee;
import com.tsystems.javaschool.challenge.dashboard.domain.Story;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class StoryControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    void getAllStories_returnsNotNull() throws Exception {
        mvc.perform(get("/story"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test
    void getStoryById_returnsExistingStory() throws Exception {
        saveStory(new Story(0, Story.Status.OPEN, 1, 1, 0, new Employee()));
        long idToRetrieve = saveStory(new Story(0, Story.Status.OPEN, 2, 2, 0, new Employee())).getId();
        saveStory(new Story(0, Story.Status.OPEN, 3, 3, 0, new Employee()));

        mvc.perform(get("/story/" + idToRetrieve))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is((int) idToRetrieve)))
                .andExpect(jsonPath("$.status", is("OPEN")))
                .andExpect(jsonPath("$.estimation", is(2)))
                .andExpect(jsonPath("$.priority", is(2)))
                .andExpect(jsonPath("$.version", is(1)));
    }

    @Test
    void getStoryById_returns404IfNotFound() throws Exception {
        saveStory(new Story(0, Story.Status.OPEN, 1, 1, 0, new Employee()));
        saveStory(new Story(0, Story.Status.OPEN, 2, 2, 0, new Employee()));
        saveStory(new Story(0, Story.Status.OPEN, 3, 3, 0, new Employee()));

        mvc.perform(get("/story/123456"))
                .andExpect(status().isNotFound());
    }

    @Test
    void save_returnsNewStoryWithId() throws Exception {
        mvc.perform(post("/story")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(new Story(0, Story.Status.OPEN, 13, 1, 0, new Employee()))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", greaterThan(0)))
                .andExpect(jsonPath("$.status", is("OPEN")))
                .andExpect(jsonPath("$.estimation", is(13)))
                .andExpect(jsonPath("$.priority", is(1)))
                .andExpect(jsonPath("$.version", is(1)));
    }

    @Test
    void update_editsStory() throws Exception {
        Story story = mapper.readValue(mvc.perform(post("/story")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(new Story(0, Story.Status.OPEN, 13, 1, 0, new Employee()))))
                .andReturn()
                .getResponse()
                .getContentAsString(), Story.class);
        assertTrue(story.getId() > 0);
        story.setStatus(Story.Status.IN_PROGRESS);
        mvc.perform(put("/story/" + story.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(story)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is((int) story.getId())))
                .andExpect(jsonPath("$.status", is("IN_PROGRESS")))
                .andExpect(jsonPath("$.estimation", is(13)))
                .andExpect(jsonPath("$.priority", is(1)))
                .andExpect(jsonPath("$.version", is(2)));
    }

    @Test
    void deleteAll() throws Exception {
        saveStory(new Story(0, Story.Status.OPEN, 1, 1, 0, new Employee()));
        saveStory(new Story(0, Story.Status.OPEN, 2, 2, 0, new Employee()));
        saveStory(new Story(0, Story.Status.OPEN, 3, 3, 0, new Employee()));

        mvc.perform(get("/story"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(greaterThan(0))));
        mvc.perform(delete("/story"))
                .andExpect(status().isNoContent());
        mvc.perform(get("/story"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    void deleteById() throws Exception {
        saveStory(new Story(0, Story.Status.OPEN, 1, 1, 0, new Employee()));
        long idToDelete = saveStory(new Story(0, Story.Status.OPEN, 2, 2, 0, new Employee())).getId();
        saveStory(new Story(0, Story.Status.OPEN, 3, 3, 0, new Employee()));

        mvc.perform(get("/story"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[?(@.id==" + idToDelete + ")]", hasSize(1)));
        mvc.perform(delete("/story/" + idToDelete))
                .andExpect(status().isNoContent());
        mvc.perform(get("/story"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[?(@.id==" + idToDelete + ")]", hasSize(0)));
    }

    private Story saveStory(Story story) throws Exception {
        return mapper.readValue(mvc.perform(post("/story")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(story)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(), Story.class);
    }

}
