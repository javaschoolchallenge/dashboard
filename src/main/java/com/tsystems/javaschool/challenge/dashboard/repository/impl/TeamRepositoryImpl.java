package com.tsystems.javaschool.challenge.dashboard.repository.impl;

import com.tsystems.javaschool.challenge.dashboard.domain.Employee;
import com.tsystems.javaschool.challenge.dashboard.repository.TeamRepository;

import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class TeamRepositoryImpl implements TeamRepository {

    Map<String, Employee> team = new HashMap<>() {{
        put("Dev1", new Employee("Dev1", Employee.Position.DEVELOPER, true));
        put("Dev2", new Employee("Dev2", Employee.Position.DEVELOPER, true));
        put("Dev3", new Employee("Dev3", Employee.Position.DEVELOPER, true));
        put("Dev4", new Employee("Dev4", Employee.Position.DEVELOPER, true));
        put("Dev5", new Employee("Dev5", Employee.Position.DEVELOPER, true));
        put("Dev6", new Employee("Dev6", Employee.Position.DEVELOPER, true));
        put("Dev7", new Employee("Dev7", Employee.Position.DEVELOPER, true));
        put("Dev8", new Employee("Dev8", Employee.Position.DEVELOPER, true));
        put("Dev9", new Employee("Dev9", Employee.Position.DEVELOPER, true));
        put("Dev10", new Employee("Dev10", Employee.Position.DEVELOPER, true));
        put("Test1", new Employee("Test1", Employee.Position.TESTER, true));
        put("Test2", new Employee("Test2", Employee.Position.TESTER, true));
        put("Test3", new Employee("Test3", Employee.Position.TESTER, true));
        put("Test4", new Employee("Test4", Employee.Position.TESTER, true));
        put("Test5", new Employee("Test5", Employee.Position.TESTER, true));
        put("Test6", new Employee("Test6", Employee.Position.TESTER, true));
        put("Test7", new Employee("Test7", Employee.Position.TESTER, true));
        put("Test8", new Employee("Test8", Employee.Position.TESTER, true));
        put("Test9", new Employee("Test9", Employee.Position.TESTER, true));
        put("Test10", new Employee("Test10", Employee.Position.TESTER, true));
        put("Analyst1", new Employee("Analyst1", Employee.Position.ANALYST, true));
    }};

    @Override
    public Employee getFreeAnalyst() {
        var freeAnalysts = team.values().stream()
                .filter(employee -> employee.getPosition().name().equalsIgnoreCase(Employee.Position.ANALYST.name()))
                .filter(Employee::isFreeStatus)
                .collect(Collectors.toList());
        return CollectionUtils.isEmpty(freeAnalysts) ? null : freeAnalysts.get(0);
    }

    @Override
    public Employee getFreeTester() {
        var freeTesters = team.values().stream()
                .filter(employee -> employee.getPosition().name().equalsIgnoreCase(Employee.Position.TESTER.name()))
                .filter(Employee::isFreeStatus)
                .collect(Collectors.toList());
        return CollectionUtils.isEmpty(freeTesters) ? null : freeTesters.get(0);
    }

    @Override
    public Employee getFreeDeveloper() {
        var freeDevelopers = team.values().stream()
                .filter(employee -> employee.getPosition().name().equalsIgnoreCase(Employee.Position.DEVELOPER.name()))
                .filter(Employee::isFreeStatus)
                .collect(Collectors.toList());
        return CollectionUtils.isEmpty(freeDevelopers) ? null : freeDevelopers.get(0);
    }

    @Override
    public void updateEmployeeStatus(Employee employee) {
        team.put(employee.getName(), employee);
    }

}
