package com.tsystems.javaschool.challenge.dashboard.repository;

public interface SprintRepository {

    boolean getStarted();
    void setStarted(boolean started);

}
