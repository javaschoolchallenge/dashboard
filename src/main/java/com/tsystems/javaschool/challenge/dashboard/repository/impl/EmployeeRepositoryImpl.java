package com.tsystems.javaschool.challenge.dashboard.repository.impl;

import com.tsystems.javaschool.challenge.dashboard.repository.EmployeeRepository;

import lombok.Data;
import org.springframework.stereotype.Repository;

import java.util.concurrent.atomic.AtomicLong;

@Data
@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository {

    private AtomicLong lastDeveloperID = new AtomicLong(0L);
    private AtomicLong lastTesterID =  new AtomicLong(0L);
    private AtomicLong lastAnalystID =  new AtomicLong(0L);

    @Override
    public long getLastDeveloperID() {
        if(lastDeveloperID.get() > 9L){
            setLastDeveloperID(new AtomicLong(0L));
        }
        return lastDeveloperID.incrementAndGet();
    }

    @Override
    public long getLastTesterID() {
        if(lastTesterID.get() > 9L){
            setLastTesterID(new AtomicLong(0L));
        }
        return lastTesterID.incrementAndGet();
    }

    @Override
    public long getLastAnalystID() {
        if(lastAnalystID.get() > 0L){
            setLastAnalystID(new AtomicLong(0L));
        }
        return lastAnalystID.incrementAndGet();
    }
}
