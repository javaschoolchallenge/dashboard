package com.tsystems.javaschool.challenge.dashboard.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EntityConcurrentModificationException extends RuntimeException {

    public EntityConcurrentModificationException() {
    }

    public EntityConcurrentModificationException(String message) {
        super(message);
    }

}
