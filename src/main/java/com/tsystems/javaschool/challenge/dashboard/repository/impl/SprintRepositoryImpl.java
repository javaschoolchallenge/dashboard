package com.tsystems.javaschool.challenge.dashboard.repository.impl;

import com.tsystems.javaschool.challenge.dashboard.repository.SprintRepository;

import org.springframework.stereotype.Repository;

@Repository
public class SprintRepositoryImpl implements SprintRepository {

    private boolean sprintStarted = false;

    @Override
    public boolean getStarted() {
        return sprintStarted;
    }

    @Override
    public void setStarted(boolean sprintStarted) {
        this.sprintStarted = sprintStarted;
    }

}
