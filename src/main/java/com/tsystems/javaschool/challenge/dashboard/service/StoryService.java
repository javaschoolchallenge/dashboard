package com.tsystems.javaschool.challenge.dashboard.service;

import com.tsystems.javaschool.challenge.dashboard.domain.Story;

import java.util.List;

public interface StoryService {

    List<Story> getAllStories();
    Story getById(long id);
    Story save(Story story);
    Story update(Story story);
    void deleteAll();
    void deleteById(long id);

}
