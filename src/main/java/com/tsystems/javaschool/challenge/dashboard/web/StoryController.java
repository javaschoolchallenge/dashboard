package com.tsystems.javaschool.challenge.dashboard.web;

import com.tsystems.javaschool.challenge.dashboard.domain.Story;
import com.tsystems.javaschool.challenge.dashboard.service.StoryService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("story")
@CrossOrigin
@RequiredArgsConstructor
public class StoryController {

    private final StoryService storyService;

    @GetMapping
    public List<Story> getAllStories() {
        return storyService.getAllStories();
    }

    @GetMapping("{id}")
    public ResponseEntity<Story> getStoryById(@PathVariable long id) {
        return ResponseEntity.ok(storyService.getById(id));
    }

    @PostMapping
    public ResponseEntity<Story> save(@RequestBody Story story) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(storyService.save(story));
    }

    @PutMapping("{id}")
    public ResponseEntity<Story> update(@PathVariable long id, @RequestBody Story story) {
        story.setId(id);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(storyService.update(story));
    }

    @DeleteMapping
    public ResponseEntity<Void> deleteAll() {
        storyService.deleteAll();
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteById(@PathVariable long id) {
        storyService.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

}
