package com.tsystems.javaschool.challenge.dashboard.repository;

public interface EmployeeRepository {

    long getLastDeveloperID();
    long getLastTesterID();
    long getLastAnalystID();
}
