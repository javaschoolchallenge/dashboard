package com.tsystems.javaschool.challenge.dashboard.service;

public interface SprintService {

    boolean getStarted();
    void startSprint();
    void stopSprint();

}
