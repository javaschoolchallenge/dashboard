package com.tsystems.javaschool.challenge.dashboard.repository.impl;

import com.tsystems.javaschool.challenge.dashboard.domain.Story;
import com.tsystems.javaschool.challenge.dashboard.exception.EntityConcurrentModificationException;
import com.tsystems.javaschool.challenge.dashboard.exception.StoryNotFoundException;
import com.tsystems.javaschool.challenge.dashboard.repository.StoryRepository;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StoryRepositoryImpl implements StoryRepository {

    private long lastId = 0L;

    private List<Story> stories = new ArrayList<>();

    @Override
    public List<Story> getAllStories() {
        return List.copyOf(stories);
    }

    @Override
    public Story getById(long id) {
        return stories.stream()
                .filter(story -> story.getId() == id)
                .findAny()
                .orElseThrow(() -> new StoryNotFoundException(id));
    }

    @Override
    public void save(Story story) {
        story.setId(++lastId);
        story.setVersion(1);
        stories.add(story);
    }

    @Override
    public Story update(Story story) {
        var existingStory = getById(story.getId());
        if (story.getVersion() == 0) {
            populate(story, existingStory);
        } else synchronized (existingStory) {
            if (story.getVersion() != existingStory.getVersion()) {
                throw new EntityConcurrentModificationException("Attempted to update a story using outdated version");
            }
            populate(story, existingStory);
        }
        return existingStory;
    }

    @Override
    public void clear() {
        stories.clear();
        lastId = 0L;
    }

    @Override
    public void deleteById(long id) {
        stories.removeIf(story -> story.getId() == id);
    }

    private void populate(Story from, Story to) {
        to.setStatus(from.getStatus());
        to.setEstimation(from.getEstimation());
        to.setPriority(from.getPriority());
        to.incrementVersion();
        to.setAssigned(from.getAssigned());
    }

}
