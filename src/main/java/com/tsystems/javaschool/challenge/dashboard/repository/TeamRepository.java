package com.tsystems.javaschool.challenge.dashboard.repository;

import com.tsystems.javaschool.challenge.dashboard.domain.Employee;

public interface TeamRepository {
    Employee getFreeAnalyst();
    Employee getFreeTester();
    Employee getFreeDeveloper();
    void updateEmployeeStatus(Employee employee);
}
