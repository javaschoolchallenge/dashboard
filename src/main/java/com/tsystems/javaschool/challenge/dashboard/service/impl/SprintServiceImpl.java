package com.tsystems.javaschool.challenge.dashboard.service.impl;

import com.tsystems.javaschool.challenge.dashboard.repository.SprintRepository;
import com.tsystems.javaschool.challenge.dashboard.service.SprintService;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SprintServiceImpl implements SprintService {

    private final SprintRepository sprintRepository;

    @Override
    public boolean getStarted() {
        return sprintRepository.getStarted();
    }

    @Override
    public void startSprint() {
        sprintRepository.setStarted(true);
    }

    @Override
    public void stopSprint() {
        sprintRepository.setStarted(false);
    }

}
