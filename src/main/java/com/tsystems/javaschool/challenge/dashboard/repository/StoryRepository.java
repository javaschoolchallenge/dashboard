package com.tsystems.javaschool.challenge.dashboard.repository;

import com.tsystems.javaschool.challenge.dashboard.domain.Story;

import java.util.List;

public interface StoryRepository {

    List<Story> getAllStories();
    Story getById(long id);
    void save(Story story);
    Story update(Story story);
    void clear();
    void deleteById(long id);

}
