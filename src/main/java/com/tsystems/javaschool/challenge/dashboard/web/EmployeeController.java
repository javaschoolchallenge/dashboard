package com.tsystems.javaschool.challenge.dashboard.web;

import com.tsystems.javaschool.challenge.dashboard.repository.EmployeeRepository;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    @GetMapping("analyst")
    public ResponseEntity<Long> getLastAnalystID() {
        return new ResponseEntity<>(employeeRepository.getLastAnalystID(), HttpStatus.OK);
    }

    @GetMapping("tester")
    public ResponseEntity<Long> getLastTesterID() {
        return new ResponseEntity<>(employeeRepository.getLastTesterID(), HttpStatus.OK);
    }

    @GetMapping("developer")
    public ResponseEntity<Long> getLastDeveloperID() {
        return new ResponseEntity<>(employeeRepository.getLastDeveloperID(), HttpStatus.OK);
    }
}
