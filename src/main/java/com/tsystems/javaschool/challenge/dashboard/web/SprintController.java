package com.tsystems.javaschool.challenge.dashboard.web;

import com.tsystems.javaschool.challenge.dashboard.service.SprintService;
import com.tsystems.javaschool.challenge.dashboard.service.StoryService;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("sprint")
@CrossOrigin
@RequiredArgsConstructor
public class SprintController {

    private final SprintService sprintService;
    private final StoryService storyService;

    @GetMapping
    public boolean getStarted() {
        return sprintService.getStarted();
    }

    @PostMapping("start")
    public ResponseEntity<Void> startSprint() {
        sprintService.startSprint();
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("stop")
    public ResponseEntity<Void> stopSprint() {
        sprintService.stopSprint();
        storyService.deleteAll();
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
