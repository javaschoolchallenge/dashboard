package com.tsystems.javaschool.challenge.dashboard.service.impl;

import com.tsystems.javaschool.challenge.dashboard.domain.Story;
import com.tsystems.javaschool.challenge.dashboard.repository.StoryRepository;
import com.tsystems.javaschool.challenge.dashboard.service.StoryService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class StoryServiceImpl implements StoryService {

    private final StoryRepository storyRepository;

    @Override
    public List<Story> getAllStories() {
        log.info("Retrieving a list of stories");
        return storyRepository.getAllStories();
    }

    @Override
    public Story getById(long id) {
        log.info("Retrieving a story with ID {}", id);
        return storyRepository.getById(id);
    }

    @Override
    public Story save(Story story) {
        log.info("Saving a story: {}", story);
        storyRepository.save(story);
        log.info("The story has been saved with ID {}", story.getId());
        return story;
    }

    @Override
    public Story update(Story story) {
        log.info("Updating a story: {}", story);
        return storyRepository.update(story);
    }

    @Override
    public void deleteAll() {
        log.info("Removing all stories");
        storyRepository.clear();
    }

    @Override
    public void deleteById(long id) {
        log.info("Removing story by ID {}", id);
        storyRepository.deleteById(id);
    }

}
