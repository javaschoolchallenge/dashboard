package com.tsystems.javaschool.challenge.dashboard.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class StoryNotFoundException extends RuntimeException {

    public StoryNotFoundException(long id) {
        super(String.format("Story with ID %d not found", id));
    }

}
